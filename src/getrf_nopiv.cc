// Copyright (c) 2017-2020, University of Tennessee. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause
// This program is free software: you can redistribute it and/or modify it under
// the terms of the BSD 3-Clause license. See the accompanying LICENSE file.

#include "slate/slate.hh"
#include "aux/Debug.hh"
#include "slate/Matrix.hh"
#include "slate/Tile_blas.hh"
#include "slate/TriangularMatrix.hh"
#include "internal/internal.hh"

namespace slate {

// specialization namespace differentiates, e.g.,
// internal::getrf_nopiv from internal::specialization::getrf_nopiv
namespace internal {
namespace specialization {

//------------------------------------------------------------------------------
/// Distributed parallel LU factorization without pivoting.
/// Generic implementation for any target.
/// Panel and lookahead computed on host using Host OpenMP task.
/// @ingroup gesv_specialization
///
template <Target target, typename scalar_t>
void getrf_nopiv(slate::internal::TargetType<target>,
           Matrix<scalar_t>& A,
           int64_t ib, int max_panel_threads, int64_t lookahead)
{
    // using real_t = blas::real_type<scalar_t>;
    using BcastList = typename Matrix<scalar_t>::BcastList;

    Layout host_layout = Layout::ColMajor;
    Layout target_layout = Layout::ColMajor;

    if (target == Target::Devices) {
        A.allocateBatchArrays();
        A.reserveDeviceWorkspace();
    }

    const int priority_one = 1;
    const int priority_zero = 0;
    int64_t A_nt = A.nt();
    int64_t A_mt = A.mt();
    int64_t min_mt_nt = std::min(A.mt(), A.nt());

    // OpenMP needs pointer types, but vectors are exception safe
    std::vector< uint8_t > column_vector(A_nt);
    std::vector< uint8_t > diag_vector(A_nt);
    uint8_t* column = column_vector.data();
    uint8_t* diag = diag_vector.data();

    #pragma omp parallel
    #pragma omp master
    {
        omp_set_nested(1);
        for (int64_t k = 0; k < min_mt_nt; ++k) {

            // panel, high priority
            #pragma omp task depend(inout:column[k]) \
                             depend(out:diag[k]) \
                             priority(priority_one)
            {
                // factor A(k, k)
                internal::getrf_nopiv<Target::HostTask>(A.sub(k, k, k, k),
                                                        ib,
                                                        priority_one);

                // Update panel
                int tag_k = k;
                BcastList bcast_list_A;
                bcast_list_A.push_back({k, k, {A.sub(k+1, A_mt-1, k, k),
                                               A.sub(k, k, k+1, A_nt-1)}});
                A.template listBcast(bcast_list_A, host_layout, tag_k);
            }

            #pragma omp task depend(inout:column[k]) \
                             priority(priority_one)
            {
                int64_t tag_k = k;
                auto Akk = A.sub(k, k, k, k);
                auto Tkk = TriangularMatrix<scalar_t>(Uplo::Upper, Diag::NonUnit, Akk);

                internal::trsm<Target::HostTask>(
                    Side::Right,
                    scalar_t(1.0), std::move(Tkk),
                                   A.sub(k+1, A_mt-1, k, k),
                    priority_one);

                BcastList bcast_list_A;
                // bcast the tiles of the panel to the right hand side
                for (int64_t i = k+1; i < A_mt; ++i) {
                    // send A(i, k) across row A(i, k+1:nt-1)
                    bcast_list_A.push_back({i, k, {A.sub(i, i, k+1, A_nt-1)}});
                }
                A.template listBcast(bcast_list_A, Layout::ColMajor, tag_k);

            }
            // update lookahead column(s), high priority
            // Done on CPU, not target.
            for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) {
                #pragma omp task depend(in:diag[k]) \
                                 depend(inout:column[j]) \
                                 priority(priority_one)
                {
                    // swap rows in A(k:mt-1, j)
                    int tag_j = j;

                    auto Akk = A.sub(k, k, k, k);
                    auto Tkk =
                        TriangularMatrix<scalar_t>(Uplo::Lower, Diag::Unit, Akk);

                    // solve A(k, k) A(k, j) = A(k, j)
                    internal::trsm<Target::HostTask>(
                        Side::Left,
                        scalar_t(1.0), std::move(Tkk),
                                       A.sub(k, k, j, j), priority_one);

                    // send A(k, j) across column A(k+1:mt-1, j)
                    // todo: trsm still operates in ColMajor
                    A.tileBcast(k, j, A.sub(k+1, A_mt-1, j, j), Layout::ColMajor, tag_j);
                }

                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[j]) \
                                 priority(priority_one)
                {
                    // A(k+1:mt-1, j) -= A(k+1:mt-1, k) * A(k, j)
                    internal::gemm<Target::HostTask>(
                        scalar_t(-1.0), A.sub(k+1, A_mt-1, k, k),
                                        A.sub(k, k, j, j),
                        scalar_t(1.0),  A.sub(k+1, A_mt-1, j, j),
                        host_layout, priority_one);
                }
            }
            // update trailing submatrix, normal priority
            if (k+1+lookahead < A_nt) {
                #pragma omp task depend(in:diag[k]) \
                                 depend(inout:column[k+1+lookahead]) \
                                 depend(inout:column[A_nt-1])
                {
                    // swap rows in A(k:mt-1, kl+1:nt-1)
                    int tag_kl1 = k+1+lookahead;

                    auto Akk = A.sub(k, k, k, k);
                    auto Tkk =
                        TriangularMatrix<scalar_t>(Uplo::Lower, Diag::Unit, Akk);

                    // solve A(k, k) A(k, kl+1:nt-1) = A(k, kl+1:nt-1)
                    // todo: target
                    internal::trsm<Target::HostTask>(
                        Side::Left,
                        scalar_t(1.0), std::move(Tkk),
                                       A.sub(k, k, k+1+lookahead, A_nt-1));

                    // send A(k, kl+1:A_nt-1) across A(k+1:mt-1, kl+1:nt-1)
                    BcastList bcast_list_A;
                    for (int64_t j = k+1+lookahead; j < A_nt; ++j) {
                        // send A(k, j) across column A(k+1:mt-1, j)
                        bcast_list_A.push_back({k, j, {A.sub(k+1, A_mt-1, j, j)}});
                    }
                    // todo: trsm still operates in ColMajor
                    A.template listBcast(bcast_list_A, Layout::ColMajor, tag_kl1);
                }

                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[k+1+lookahead]) \
                                 depend(inout:column[A_nt-1])
                {
                    // A(k+1:mt-1, kl+1:nt-1) -= A(k+1:mt-1, k) * A(k, kl+1:nt-1)
                    internal::gemm<target>(
                        scalar_t(-1.0), A.sub(k+1, A_mt-1, k, k),
                                        A.sub(k, k, k+1+lookahead, A_nt-1),
                        scalar_t(1.0),  A.sub(k+1, A_mt-1, k+1+lookahead, A_nt-1),
                        target_layout, priority_zero);
                }
            }
        }
    }

    #pragma omp parallel
    #pragma omp master
    {
        A.tileLayoutReset();
    }
    A.clearWorkspace();
}

} // namespace specialization
} // namespace internal

//------------------------------------------------------------------------------
/// Version with target as template parameter.
/// @ingroup gesv_specialization
///
template <Target target, typename scalar_t>
void getrf_nopiv(Matrix<scalar_t>& A,
           Options const& opts)
{
    int64_t lookahead;
    try {
        lookahead = opts.at(Option::Lookahead).i_;
        assert(lookahead >= 0);
    }
    catch (std::out_of_range&) {
        lookahead = 1;
    }

    int64_t ib;
    try {
        ib = opts.at(Option::InnerBlocking).i_;
        assert(ib >= 0);
    }
    catch (std::out_of_range&) {
        ib = 16;
    }

    int64_t max_panel_threads;
    try {
        max_panel_threads = opts.at(Option::MaxPanelThreads).i_;
        assert(max_panel_threads >= 1 && max_panel_threads <= omp_get_max_threads());
    }
    catch (std::out_of_range&) {
        max_panel_threads = std::max(omp_get_max_threads()/2, 1);
    }

    internal::specialization::getrf_nopiv(internal::TargetType<target>(),
                                    A,
                                    ib, max_panel_threads, lookahead);
}

//------------------------------------------------------------------------------
/// Distributed parallel LU factorization without pivoting.
///
/// Computes an LU factorization without pivoting of a general m-by-n matrix $A$
///
/// The factorization has the form
/// \[
///     A = L U
/// \]
/// where $L$ is lower triangular with unit diagonal elements
/// (lower trapezoidal if m > n), and $U$ is upper triangular
/// (upper trapezoidal if m < n).
///
/// This is the right-looking Level 3 BLAS version of the algorithm.
///
//------------------------------------------------------------------------------
/// @tparam scalar_t
///     One of float, double, std::complex<float>, std::complex<double>.
//------------------------------------------------------------------------------
/// @param[in,out] A
///     On entry, the matrix $A$ to be factored.
///     On exit, the factors $L$ and $U$ from the factorization $A = P L U$;
///     the unit diagonal elements of $L$ are not stored.
///
/// @param[in] opts
///     Additional options, as map of name = value pairs. Possible options:
///     - Option::Lookahead:
///       Number of panels to overlap with matrix updates.
///       lookahead >= 0. Default 1.
///     - Option::InnerBlocking:
///       Inner blocking to use for panel. Default 16.
///     - Option::Target:
///       Implementation to target. Possible values:
///       - HostTask:  OpenMP tasks on CPU host [default].
///       - HostNest:  nested OpenMP parallel for loop on CPU host.
///       - HostBatch: batched BLAS on CPU host.
///       - Devices:   batched BLAS on GPU device.
///
/// TODO: return value
/// @retval 0 successful exit
/// @retval >0 for return value = $i$, $U(i,i)$ is exactly zero. The
///         factorization has been completed, but the factor $U$ is exactly
///         singular, and division by zero will occur if it is used
///         to solve a system of equations.
///
/// @ingroup gesv_computational
///
template <typename scalar_t>
void getrf_nopiv(Matrix<scalar_t>& A,
           Options const& opts)
{
    Target target;
    try {
        target = Target(opts.at(Option::Target).i_);
    }
    catch (std::out_of_range&) {
        target = Target::HostTask;
    }

    switch (target) {
        case Target::Host:
        case Target::HostTask:
            getrf_nopiv<Target::HostTask>(A, opts);
            break;
        case Target::HostNest:
            getrf_nopiv<Target::HostNest>(A, opts);
            break;
        case Target::HostBatch:
            getrf_nopiv<Target::HostBatch>(A, opts);
            break;
        case Target::Devices:
            getrf_nopiv<Target::Devices>(A, opts);
            break;
    }
    // todo: return value for errors?
}

//------------------------------------------------------------------------------
// Explicit instantiations.
template
void getrf_nopiv<float>(
    Matrix<float>& A,
    Options const& opts);

template
void getrf_nopiv<double>(
    Matrix<double>& A,
    Options const& opts);

template
void getrf_nopiv< std::complex<float> >(
    Matrix< std::complex<float> >& A,
    Options const& opts);

template
void getrf_nopiv< std::complex<double> >(
    Matrix< std::complex<double> >& A,
    Options const& opts);

} // namespace slate
